import React, { Component } from 'react';
import Curso from './Curso';

const Cursos = require('./data.json');

export default class Conocimientos extends Component {
	constructor(props) {
		super(props);
			this.state = { Cursos: Cursos.Cursos };
	}
	render() {
		const { Cursos } = this.state;
		return (
			<div>
				<div className="container"><h2 className="Curso-list-title">Mis Conocimientos</h2></div>
				<section id="Conocimientos" className="Curso-list">
					<div className="container">
              <h4 className="Curso-list-title">Carreras</h4>
            <div className="Cursos-container">
                {
                  Cursos.map((Data) => {
                    if(Data.Type ==="Carr"){
                      return <Curso data={Data}/>
                    } return (<div/>)
                  })
                }
              </div>
            <h4 className="Curso-list-title">Cursos</h4>
            <div className="Cursos-container">
            {
              Cursos.map((Data) => {
                if (Data.Type === "Curso") {
                  return <Curso data={Data} />
                } else return (<div/>)
              })
            }
            </div>
            <h4 className="Curso-list-title">Cursos Obsoletos</h4>
            <div className="Cursos-container">
              {
                Cursos.map((Data) => {
                  if (Data.Type === "Dep") {
                    return <Curso data={Data} />
                  } else return (<div/>)
                })
              }
            </div>
					</div>
				</section>
			</div>
		)
	}
}
